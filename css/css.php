<style>
/* FONT */
@font-face {
	font-family: rob-m;
	src: url('fonts/roboto-medium.eot?#iefix') format('embedded-opentype'),  
		   url('fonts/roboto-medium.woff') format('woff'), 
		   url('fonts/roboto-medium.ttf')  format('truetype'), 
		   url('fonts/roboto-medium.svg#roboto-medium') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: rob-r;
	src: url('fonts/roboto-regular.eot?#iefix') format('embedded-opentype'),  
		   url('fonts/roboto-regular.woff') format('woff'), 
		   url('fonts/roboto-regular.ttf')  format('truetype'), 
		   url('fonts/roboto-regular.svg#roboto-regular') format('svg');
	font-weight: normal;
	font-style: normal;
  }
@font-face {
	font-family: rob-l;
	src: url('fonts/roboto-light.eot?#iefix') format('embedded-opentype'),  
		   url('fonts/roboto-light.woff') format('woff'), 
		   url('fonts/roboto-light.ttf')  format('truetype'), 
		   url('fonts/roboto-light.svg#roboto-light') format('svg');
	font-weight: normal;
	font-style: normal;
}

/* RESET */
body, ul, li, ol, form, h1, h2, h3, h4, h5, h6, div, span, p { padding:0; margin:0; border:0; -webkit-text-size-adjust:none; -moz-text-size-adjust:none; text-size-adjust:none;}
article, aside, footer, header, main, nav, section           { display:block;}  
input, textarea, select { -webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none; border-radius:0;}
strong,b				{ font-weight:bold;}
em				        { font-style: normal; font-family:rob-l;}
ul						{ list-style-type:none;}
body					{ font:normal 16px/30px rob-l; color:#4a4a4a; background:#fff;}
a						{ text-decoration:none; color:#da3013; outline:none;}
img						{ border:none;}
#wrapper 				{ min-width:320px; overflow:hidden; position:relative;}
#wrapper *				{ box-sizing:border-box; outline:none;}
.wrap					{ max-width:1200px; width:100%; margin:0 auto; padding:0 40px; position:relative;} 

/* CLASS */
.clear			{ clear:both; float:none !important; width:100% !important; padding:0 !important; margin:0 !important; display:block;}
.left			{ float:left;}
.right 			{ float:right;}
.titre 			{ margin-bottom: 5px; font:normal 36px/36px rob-l; letter-spacing: .5px; text-transform:uppercase; color:#4a4a4a; text-transform: uppercase;}
.stitre 		{ margin-bottom: 20px; font:normal 18px/36px rob-l; color:#6eaa28; letter-spacing:.25px;}
.sousTitre 		{ padding-bottom:20px; font:normal 18px/36px rob-l; color:#6eaa28; letter-spacing:.25px; position: relative;}
.link 			{ width:auto; height:55px; display:inline-block; position:relative; font:normal 16px/30px rob-l; letter-spacing: .5px; color:#fff; text-decoration:none !important; padding:12px 30px 0 30px; background:#437f34;}
.clr:after 		{ content:''; display:table; clear:both;}

/* HEADER */
#header					{ width:100%; position:relative;}
.headerTop				{ width:100%; min-height:150px; position:relative; z-index:1000; padding:0; border-bottom: 2px solid #fff;}
.logo-slogan 			{ background: #437f34; width: 320px; padding: 30px 0; position: absolute; text-align: center;}
.headerTop .logo		{ display:block; height:auto; line-height: 0;}
.headerTop .right 		{ width: 75%;}
.headerTop .right .tel 	{ background:#255519; height: 55px; float: right; margin-left: 20px; padding:0 40px 0 0; position: relative; color: #fff; font: 16px/36px rob-m;}
.headerTop .right 
.tel:before 	      	{ content: ""; background: url(images/icone-tel.svg) 50% no-repeat #437f34; width: 60px; height: 55px; display: inline-block; vertical-align: middle; margin: 0 40px 0 0; }
.slogan 				{ max-width: 405px; float: right; padding-top: 12px; font:16px/30px rob-m; color: #503831;}

.r-sociaux 				{ height: 55px; float: right; margin:20px 40px 0 80px; position: relative; z-index: 50;}
.r-sociaux .rs 			{ display: inline-block; vertical-align: middle; width: 55px; height: 55px; font-size: 0; margin-left: 8px; border: 1px #6eaa28 solid;}
.r-sociaux .lettre	    { background:url(images/icone-lettre.svg) 50% no-repeat;}
.r-sociaux .facebook	{ background:url(images/icone-facebook.svg) 50% no-repeat;}

.headerNav 				{ width:auto; height:55px; float: right; z-index:100;}
.menu 					{ width:100%; display:block; position:relative; text-align:right; font-size:0; line-height:0;}	
.menu li				{ display:inline-block; position:relative; margin:0 22px;}
.menu>ul>li:nth-child(5){ margin-right: 0;}
.menu>ul>li.social  	{ display: none;}
.menu>ul>li>a           { height:96px; padding:33px 0;}
.menu a					{ width:auto; font:normal 16px/30px rob-l; color:#4a4a4a; letter-spacing: .5px; display:block; position:relative; }
.menu>ul>li.active>a    { color:#437f34; font-family: rob-m;}

/* SUB MENU */
.sub					{ width:350px; position:absolute; top:95px; left:50%; z-index:995; margin-left:-170px; display:block; background:#255519; visibility:hidden; opacity:0; border: 1px solid #fff; border-top: none; }
.sub li					{ width:100%; margin:0; border:none; padding:0; text-align:center;}
.sub li a				{ width:100%; height:54px; position:relative; border-bottom:1px solid #fff; margin:0; padding:10px 0; color: #fff;}
.sub li:last-child a	{ border:0;}
.menu li:hover .sub 	{ opacity:1; visibility:visible; z-index:999;}
.menu .sub li.active a	{ background: #6eaa28;}

/* HIDE MOBILE */
.wrapMenuMobile, .menu .vueMobile { display:none;}

@media (min-width:1201px){ .menu ul { display:block !important;}}

/* RESPONSIVE */
@media (max-width:1200px){
  .wrapMenuMobile				{ height:50px; display:block; z-index:80; color:#437f34; font:normal 16px/30px rob-m; letter-spacing: .5px; cursor:pointer; padding:16px 20px 0 40px; text-transform:uppercase; position:absolute; left:0; top:0;}
  .menuMobile					{ width:20px; display:block; height:18px; padding-left:35px; cursor:pointer; position:relative; line-height:20px;}
  .menuMobile>div				{ width:20px; height:2px; background:#437f34; position:absolute; left:0; top:50%; margin-top:-1px;}
  .menuMobile>div:before		{ width:100%; height:2px; background:#437f34; position:absolute; left:0; top:6px; content:"";}
  .menuMobile>div:after			{ width:100%; height:2px; background:#437f34; position:absolute; left:0; top:-6px; content:"";}
  .menuMobile.active>div		{ height:0px;}
  .menuMobile.active>div:before	{ top:0; transform:rotate(45deg);}
  .menuMobile.active>div:after	{ top:0; transform:rotate(-45deg);}
  .menu 						{ width:100%; height:0; position: absolute; left: 0; z-index: 100; top: 50px; border-top: 1px #437f34 solid; text-align: left;}
  .menu>ul 						{ width:100%; height:auto; position:absolute; left:0; top:0; padding:0; background:#fff; z-index:999; display:none;}
  .menu>ul:after 				{ content: ""; width: 100%; height: 9999px; background: rgba(0,0,0,.6); position: absolute; left: 0; bottom: -9999px; z-index: -1;}
  .menu ul li 					{ width:100%; margin:0 auto; display:block; float:none; padding:0;}
  .menu>ul>li:after 			{ display: none;}
  .menu>ul>li>a					{ height:53px; line-height:53px; border:0; padding:0 40px; margin:0; font-size:16px; text-align:left; border-bottom:1px solid #000;}
  .menu>ul>li.active>a 			{ color:#437f34;}
  .menu i 						{ display:block; width:100%; height:53px; position:absolute; right:0; top:0;}
  .menu i:before 				{ display:block; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat; transform:rotate(180deg);}
  .menu i:after 				{ display:none; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat;}
  .menu i.active:before, .menu li.active i:before{ display:none;}
  .menu i.active:after, .menu li.active i:after{ display:block;}
  .menu>ul>li.active>a:after 	{ display: none;}
  .menu .vueMobile 				{ display:block;}
  .menu .sub 					{ display:none; visibility:visible; width:100%; position:relative; left:auto; top:auto; margin:0; opacity:1; background:#255519; padding:0; border:none;}
  .menu .sub li a				{ border-bottom:1px #eee solid; font-size:16px; color:#fff; text-align:left; padding:12px 40px 0; font-family: rob-l;}
  .menu .sub li.active a		{ background:#74c60c; color: #fff;}
  .menu .social 				{ display: block;}
  .headerTop .right             { float: none; position: absolute; width: 100%; top: 0;}
  .headerTop .right .tel::before{ background-color:#437f34; width: 50px; height: 50px; margin: -1px 40px 0 0;}
  .headerTop .right .tel        { height: 50px; margin-left: 20px; padding: 1px 40px 0 0; font: 16px/36px rob-m;}
  .r-sociaux                    { display: none;}
  .headerTop                    { min-height: 178px; border:none;}
  .logo-slogan                	{ width: 100%; top: 50px; right: 0; left: 0;}
  .logo-slogan                	{ padding: 20px 0;}
  .logo-slogan .slogan        	{ max-width: 100%; text-align: center;}
}
@media (max-width:980px){
	.slogan                     { display: none;}
}
@media (max-width:600px) {	
  .headerTop .right .tel        { padding: 0 20px 0 0;}
  .headerTop .right .tel::before{ margin: 0 20px 0 0;}
	.wrapMenuMobile				{ padding:16px 20px 0 20px;}
  .menu > ul > li > a           { padding: 0 20px;} 
  .menu .sub li a               { padding:10px 20px 0;}
  .menu i::before, 
  .menu i::after	            { right:20px;}
  .menu .social 				{ padding: 16px 20px;}
}

/* Homepage */
<?php if ($namePage == "pageAccueil") { ?>
	/* SLIDER */
	#slider               	{ background:url(images/banner-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover; -moz-background-size:cover; background-size:cover; width:100%; height:590px; position:relative; z-index:45;}
	#slider .slick-list   	{ width:100%; height:100%;}
	#slider .slick-track  	{ width:100%; height:100%;}
	.bannerTxt            	{ background:#255519; max-width: 640px; width: 100%; position: absolute; bottom: 0; left: 0; right: 0; margin: 0 auto; padding-top: 37px; text-align: center;}
	.bannerTxt .ban-txt    	{ display: block; margin-bottom: 4px; font: normal 36px/36px rob-m; color: #fff; letter-spacing: 0;}
    .bannerTxt .ban-txt span{ font: normal 20px/36px rob-l; letter-spacing: .25px; color: #fff; display: block;}
	.bannerTxt .link       	{ margin: 30px auto 0; padding: 12px 35px 0;}
	.bannerTxt .link:after  { content: ''; background: url(images/arrow-banner.svg) no-repeat; width: 9px; height: 11px; display: inline-block; vertical-align: middle; margin-left: 20px;}

	.blocPresentation 		{ padding: 100px 100px 50px;}
	.blocPresentation>div 	{ float: none; display: inline-block; vertical-align: middle;}
	.blocPresentation .left { max-width: 500px; width: 50%; height: auto; margin-right: 52px;}
	.blocPresentation .left img { width: 100%; height: auto; display: block;}
	.blocPresentation .right{ max-width: 441px; width: 50%; margin-top: -7px;}
	.blocPresentation .right .link{ margin-top: 30px;}

	.blocService 			{ padding-bottom: 85px; text-align: center; position: relative;}
	.blocService .wrap      { padding: 0 !important;}
	.blocService:after 		{ content: ''; background: #437f34; width: 100%; height: 5px; position: absolute; top: 50px; left: 0;}
	.slide-service 			{ padding-bottom: 40px; position: relative; z-index: 50;}
	.service>a              { display: block; margin:0 45px !important;}
	.blocService .service-image	{ display: block; background-color: #fff !important; border-radius: 50%; width: 100px; height: 100px; margin: 0 auto; margin-bottom: 17px; position: relative;}
    .service1>a>.service-image:before { background: url(images/service1-hov.svg) 50% no-repeat;}
    .service2>a>.service-image:before { background: url(images/service2-hov.svg) 50% no-repeat;}
    .service3>a>.service-image:before { background: url(images/service3-hov.svg) 50% no-repeat;}
    .service4>a>.service-image:before { background: url(images/service4-hov.svg) 50% no-repeat;}
    .service> a>.service-image:before { content: ''; background-color: #437f34 !important; position: absolute; top: 0; left: 0; z-index: 100; border-radius: 50%; width: 100px; height: 100px; opacity: 0;}
    .service-image img      { display: block; position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto;}
	.blocService .titre 	{ display: block; margin-bottom: 28px; font: normal 20px/22px rob-m; letter-spacing: .25px; text-transform: inherit !important;}
	.blocService p 			{ color: #4a4a4a !important;}
	.blocService .link 		{ margin-top: 30px; padding: 12px 30px 0 !important; display: inline-block !important;}

	.blocAtout 				{ background: #f2f1f1; height: 265px; position: relative; font:normal 16px/20px rob-l; text-align: center;}
	.atout 					{ height: 265px !important;}
	.blocAtout span 		{ display: block; font:normal 18px/20px rob-r; color:#6eaa28; letter-spacing: .25px;}
	.blocAtout .picto 		{ background: #fff; width: 100px; height: 100px; margin: 0 auto; border-radius: 50%; border: 1px #6ec600 solid; position: relative; margin-top: 50px; margin-bottom: 25px;}
	.blocAtout .picto img 	{ display: block; position: absolute; top: 0; right: 0; bottom: 0; left: 0; margin: auto;}
	.blocAtout .slick-prev  { width: 55px; height: 55px; background: url(images/arrow-left.svg) 50% 50% no-repeat; position: absolute; left: 0 !important; bottom: 0; z-index: 40; cursor: pointer; text-indent: -9999px; border: 0; top: 50%; margin-top: -70px;}
	.blocAtout .slick-next  { width: 55px; height: 55px; background: url(images/arrow-right.svg) 50% 50% no-repeat; position: absolute; right: 0 !important; bottom: 0; z-index: 40; cursor: pointer; text-indent: -9999px; border: 0; top: 50%; margin-top: -70px;}

	.bloc-real-coor 		{ position: relative;}
	.blocContact 			{ background: #437f34; padding: 50px 0; text-align: right;}
	.blocContact>div 		{ padding-right:100px;}
	.blocContact>div>a		{ display: inline-block; vertical-align: top; background:#255519; height: 55px; padding:10px 40px 0 30px; position: relative; color: #fff; font: 16px/36px rob-m; position: relative;}
	.blocContact .phone:before{ content: ''; background: url(images/icone-tel.svg) 50% no-repeat; width: 15px; height: 15px; display: inline-block; vertical-align: middle; margin-right: 15px; margin-top: -2px;}
	.blocContact .mail:before{ content: ''; background: url(images/icone-lettre2.svg) 50% no-repeat; width: 15px; height: 11px; display: inline-block; vertical-align: middle; margin-right: 15px; margin-top: -2px;}
    .blocContact .mail      { margin-left: 35px; padding: 10px 32px 0 22px}
	.bloc-real-coor .left 	{ max-width: 500px; width: 100%; height: 350px; position: absolute; left: 100px; top: -105px; -webkit-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.1);-moz-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.1); box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.1);}
	.slide-real 			{ max-width: 500px; width: 100%; height: 350px;}
	.bloc-real-coor 
    .left .link             { background: #6eaa28; position: absolute; right: 0; top: 0; z-index: 75;}
	.bloc-real-coor 
    .left img               { width: 100%; height: auto; display: block;}
	.bloc-real-coor .right 	{ width: 430px; margin-right: 70px; padding: 30px 0 80px; text-align: center;}
	.bloc-real-coor 
    .right .titre           { font-size: 24px; margin-bottom: -4px;}
    .bloc-real-coor 
    .right .stitre          { font-family: rob-m;}

	/* RESPONSIVE */
	@media (max-width:1366px){
		.blocAtout .slick-prev 			{ left:0}
		.blocAtout .slick-next 			{ right:0}	
	}
	@media (max-width: 1200px){
		#slider             			{ background-image: url(images/banner-1200.jpg); height: 500px;}

		.blocPresentation               { padding: 70px 40px 50px;}
		.blocPresentation .left         { max-width: 50%; margin-right: 0;}
		.blocPresentation .right        { max-width: 45%;  width: 45%; margin-left: 4%;}
		.blocContact                    { text-align: center;}
		.blocContact>div                { padding:0 40px !important;}
		.bloc-real-coor .wrap           { padding: 50px 40px;}
		.bloc-real-coor .wrap>div       { float: none; display: inline-block; vertical-align: middle;}
		.bloc-real-coor .left           { width: 50%; height: auto; position: relative; top: 0; left: 0;}
		.slide-real                     { max-width: 100%; width: 100%; height: auto;}
		.bloc-real-coor .right          { width: 45%; margin-left: 4%; margin-right: 0;}
        .blocAtout                      { height: 240px;}
		.blocAtout .picto               { margin-top: 40px;}
	}
	@media (max-width: 980px){
		.blocPresentation               { padding: 43px 40px 50px;}
		.blocPresentation > div         { display: block;}
		.blocPresentation .left         { max-width: 80%; width: 100%; margin: 0 auto;}
		.blocPresentation .right        { max-width: 100%; width: 100%; margin-left: 0; margin-top: 40px; text-align: center;}
	}
	@media (max-width: 760px){
		#slider 						{ background-image: url(images/banner-800.jpg); height:460px;}
        .bannerTxt                    	{ max-width: 100%;  width: 100%; padding: 20px;}
		.bannerTxt .ban-txt            	{ font: normal 18px/24px rob-m;}
        .bannerTxt .ban-txt span        { font: normal 16px/20px rob-l;}
        .bannerTxt .link              	{ display: none;}

		.bloc-real-coor .wrap > div     { max-width: 100%; display: block;}
		.bloc-real-coor .left           { width: 80%; margin: 0 auto;}
		.bloc-real-coor .right          { width: 100%; margin-left: 0; margin-right: 0; padding: 30px 0 0;}
	}
	@media (max-width: 600px){
		.blocPresentation .left         { display: none;}
		.blocPresentation .right        { margin-top: 0;}
		.blocService                    { padding-bottom: 55px; border-top: 5px #437f34 solid;}
		.service > a                    { padding: 0 20px !important;}
		.blocService::after             { display: none;}
		.blocService .service-image     { margin-bottom: 0;}
		.blocService .titre             { margin-bottom: 17px;}
		.bloc-real-coor .wrap           { padding: 0;}
		.bloc-real-coor .left           { width: 100%;}
		.blocContact,
		.bloc-real-coor .right          { display: none !important;}
        .bloc-real-coor .left           { height: 350px; background: url(images/img-real.jpg) no-repeat; background-size: cover;}
		.blocContact, .slide-real, 
		.bloc-real-coor .right          { display: none !important;}
        .bloc-real-coor .left 	        { -webkit-box-shadow: none;-moz-box-shadow: none; box-shadow: none;}
		.bloc-real-coor .left .link     { bottom: 0; left: 0; right: 0; width: 210px; margin: auto; text-align: center; text-transform: capitalize;}
		.bloc-real-coor .left .link span{ display: none;}
	}
	@media (max-width: 400px){
		.blocPresentation .titre        { margin-bottom: 35px; padding-bottom: 23px; position: relative; line-height: 40px;}
		.blocPresentation .titre:after  { content: ''; background: #437f34; width: 80px; height: 1px; position: absolute; left: 0; right: 0; bottom: 0; margin: 0 auto;}
		.blocPresentation .stitre       { font: normal 18px/24px rob-l;}
		.blocPresentation .stitre span  { display: none;}
		.blocPresentation .right p      { display: none;}
		.blocPresentation .right .link  { margin-top: 14px;}
        .bloc-real-coor .left           { height: 210px;}
        .service>a                      { margin: 0 !important;}
	}
<?php } ?>

/* Pages Intérieures */
<?php if ($namePage != "pageAccueil") { ?>
	#slider 				{ background: url(images/banner-page-1800.jpg) no-repeat; -webkit-background-size:cover; -moz-background-size:cover; background-size:cover; width:100%; height: 270px;}
	.menu li:hover .sub     { border: 1px #fff solid; border-top: none;}
	.bredCrumb 				{ margin-bottom: 50px; padding-left: 0;}
	.bredCrumb a  			{ color: #4a4a4a; line-height: 60px; letter-spacing: .75px;}
	.bredCrumb span 		{ margin: 0 8px; font: normal 16px/30px rob-l;}
	.bredCrumb .active 		{ color:#58352b; font: normal 16px/30px rob-m; position: relative;}
    .bredCrumb .active:after{ display: none;}
    .bredCrumb a:after      { content: '>'; display: inline-block; vertical-align: middle; margin: 0 5px 0 8px; margin-top: -2px; font-family: rob-l;}
	.titrePage 				{ margin-bottom:25px; font:normal 36px/36px rob-l; letter-spacing: .25px; color:#4a4a4a; text-transform: uppercase; position: relative;}
	.titrePage span 		{ display: block; margin-top: 4px; font: 18px/36px rob-l; letter-spacing: .25px;color: #6eaa28; text-transform: initial !important;}
    .titrePage .sousTitre:after { width: 60px;}
    .titrePage .sousTitre:after{ content: ''; background: #6eaa28; width: 80px; height: 1px; position: absolute; bottom: 0; left: 0;}
	.sousTitre 				{ font:normal 20px/30px rob-m; color: #6eaa28; letter-spacing: .25px;}
    .chapo                  { margin-bottom: 20px; font: normal 18px/30px rob-m; color: #4a4a4a;}
    .pageContent p          { padding: 0 0 30px 0;}
    
	.blocTop 		        { padding: 20px 0 0;}
    .blocTxte1              { margin-bottom: 30px;}
	.blocPhoto 		        { width: 100%; position: relative;}
    .blocPhoto>div          { width: 50%; float: none;}
	.blocPhoto .left        { width: 562px; height:484px; overflow: hidden; display: inline-block; vertical-align: middle;}
	.blocPhoto .slide-photo { width: 562px; height:484px;}
	.blocPhoto .left img    { display: block; width: 100%; height: auto;}
	.blocPhoto .slick-prev  { width: 65px; height: 65px; background: url(images/icone-arrow-left.svg) 50.5% 50% no-repeat #255519; position: absolute; right:65px !important; bottom: 0; z-index: 40; cursor: pointer; text-indent: -9999px; border: 0;}
    .blocPhoto .slick-prev:after { content: ''; background: #63845a; width: 1px; position: absolute; top: 10px; bottom: 10px; right: 0;}
	.blocPhoto .slick-next  { width: 65px; height: 65px; background: url(images/icone-arrow-right.svg) 49.7% 50% no-repeat #255519; position: absolute; right:0 !important; bottom: 0; z-index: 40; cursor: pointer; text-indent: -9999px; border: 0;}
	.blocPhoto .right       { background: #f4f4f4; width: 60%; padding: 69px 60px 59px; position: relative; z-index: 10; display: inline-block; vertical-align: middle; margin-left: -133px;}
    .blocPhoto .right p:last-of-type{ padding-bottom: 0;}
	.blocPhoto .right .sousTitre { font: normal 20px/26px rob-m; color: #503831; letter-spacing: 0;}

	.blocContact 			{ background: #437f34; margin: 80px 0; padding: 50px 0;}
	.blocContact .wrap 		{ font-size:0; line-height: 0;}
	.blocContact span 		{ display: inline-block; vertical-align: middle;}
	.blocContact .txt-left	{ width: 40%; text-align: left; font: normal 20px/30px rob-m; letter-spacing: .5px; color: #fff;}
	.blocContact .txt-right { width: 60%; text-align: right;}
	.blocContact .txt-right>a{ display: inline-block; vertical-align: top; background:#255519; height: 55px; padding:10px 40px 0 30px; position: relative; color: #fff; font: 16px/36px rob-m; position: relative;}
	.blocContact .phone:before{ content: ''; background: url(images/icone-tel.svg) 50% no-repeat; width: 15px; height: 15px; display: inline-block; vertical-align: middle; margin-right: 15px; margin-top: -2px;}
	.blocContact .mail:before{ content: ''; background: url(images/icone-lettre2.svg) 50% no-repeat; width: 15px; height: 11px; display: inline-block; vertical-align: middle; margin-right: 15px; margin-top: -2px;}
	.blocContact .mail 		{ margin-left: 50px; padding: 10px 35px 0 25px !important;}

	.blocText 		        { margin-bottom: 30px;}
    .pageContent ul      	{ padding:0 0 0 25px}
    .pageContent ul>li   	{ padding-left: 40px; margin: 20px 0; background: url('images/icone-liste.svg') 0 10px no-repeat;}
    .pageContent ul>li:first-child{ margin-top: -5px;}

	.blocGalerie 			{ max-width: 1140px; margin: 25px auto 80px; text-align: center;}
	.bloc 					{ width: 360px; height: 313px; float: left; padding:0 10px; overflow: hidden; position: relative;}
    .bloc a  				{ display: block; position: relative;}
	.bloc a img 			{ display: block; width: 100%; height: auto;}
	.bloc .txt		 		{ background: #437f34; width: 100%; height: 75px; padding-top: 25px; position: absolute; left: 0; right: 0; bottom: 0; z-index: 10; font: normal 20px/24px rob-m; letter-spacing: .25px; color: #fff; opacity: 1; text-align: center;}
	.bloc .hide 			{ background: rgba(38, 38, 38, .8); padding: 70px 40px 0; width: 100%; position: absolute; top: 0; bottom: 0; left: 0; right: 0; opacity: 0;}
	.bloc .hide strong 		{ font: 20px/24px rob-m; letter-spacing: .75px; color: #6eaa28;}
	.bloc .hide p 			{ color: #fff !important;}
	.bloc .hide em 			{ display: block; background: url(images/icone-plus.svg) 50% no-repeat; width: 50px; height: 50px; margin: 0 auto; margin-top: 5px;}
    .slick-dots				{ padding:20px 0 !important;}
    .slick-dots	li			{ margin:0 8px !important; padding: 0 !important; background: none !important;}
	
	/* RESPONSIVE */
	@media (max-width:1200px) {
		.menu li:hover .sub 			{border: none;}
		.pageContent					{ padding:17px 0 0; min-height:0;}
		.bredCrumb 						{ margin-bottom: 12px;}
		.titrePage 						{ padding-bottom: 0;}
        .blocGalerie                    { padding: 0 20px;}
		.bloc                           { margin: 0 10px;}
		.bloc .hide                     { display: none;}
        .blocPhoto .left 				{ display: block; margin: 0 auto;}
		.blocPhoto .right 				{ width: 100%; padding:35px 40px; position: relative; margin-top: 30px; margin-left: 0;}
        .blocContact                    { margin: 40px 0 30px;}
        .slide-galerie                  { margin-left: 0;}
	}
	@media (max-width:1024px){
		.blocContact span 				{ width: 100% !important; display: block; text-align: center !important;}
		.blocContact .txt-right 		{ margin-top: 30px;}
	}
	@media (max-width:980px) { 
		.bloc a img 					{ display: block; width: 100% !important; height: auto !important;}
        .blocContact                    { margin: 40px 0 30px;}
	}
	@media (max-width:760px) {
		.blocPhoto .left 				{ width: 100%; height: auto; margin-bottom: 0;}
		.blocPhoto .slide-photo 		{ width: 100%; height: auto;}
        .pageContent ul 				{ padding: 0 0 10px;}
	    .pageContent ul >li 			{margin: 13px 0;}
	}
	@media (max-width:600px) {
        #slider                         { display: none;}
        .wrap 							{ padding: 0;}
        .pageContent 					{ border-bottom: 1px #fff solid;}
        .blocTop 						{ padding: 0 20px 0;}
        .blocText 						{ padding: 0 20px; margin-bottom: 0;}
        .chapo span, .blocContact, 
        .pageContent ul >li >span, 
        .blocText span					{ display: none;}
        .blocTop .chapo+.sousTitre, .blocText1{ display: none;}
        .blocText2 						{ padding-top: 25px;}
        .blocText2 .sousTitre             { line-height: 30px;}
        .bloc 							{ width: 100%; margin: 0; padding: 0;} 
        .blocGalerie 					{ margin: 0; padding: 0;}
        .blocPhoto .right 				{ padding: 35px 20px 15px; margin: 0;}
        .blocPhoto .slick-prev 			{ width: 60px; height: 60px; right: 0; top: 50%; margin-top: -30px; left: 0 !important;}
        .blocPhoto .slick-prev:after    { display: none;}
        .blocPhoto .slick-next 			{ width: 60px; height: 60px; right: 0; top: 50%; margin-top: -30px;}
        .slick-dots                     { position: relative !important; padding: 13px 0 25px !important;}
    }
    @media (max-width: 400px){
        .blocText .left > ul            { padding-left: 13px;}
    }
<?php } ?>

/* FOOTER */
#footer					{ background:#437f34; width:100%; height:auto; position:relative;}
.footer 				{ max-width:1120px; width:100%; margin:0 auto; padding:60px 40px 80px; position:relative;}
.footer1 				{ text-align:center;}
.logo-foot 				{ display:block; width:216px; height:50px; margin:0 auto; margin-bottom:7px; font: normal 18px/24px rob-l; letter-spacing: .25px; color: #fff !important;}
.logo-foot span 		{ display: block; font-size: 20px; text-transform: uppercase; letter-spacing: .5px; color: #fff;}
.footer ul 				{ text-align: center;}
.footer ul>li			{ display: inline-block; vertical-align: middle;}
.footer1 div 			{ font: normal 14px/30px rob-l; color: #fff;}

.social 			    { position:absolute; left:0; top:65px;}
.rs 					{ width:55px; height:55px; text-indent:-9999px; border:1px #6eaa28 solid;}
.social a 				{ width:55px; height:55px; text-indent:-9999px; display:block; float:left; margin-right:8px; cursor:pointer;}
.social .facebook		{ background:url(images/icone-facebook2.svg) 50% 50% no-repeat;}
.scroll 				{ display:block; background:url(images/icone-scroll.svg) 50% no-repeat; position:absolute; top:65px; left:inherit; right:0;}
.open-plan 				{ font: normal 14px/30px rob-l; color: #fff; cursor:pointer;}

.footer2 				{ margin-top: 20px;}
.footer2>ul>li, 
.footer2>ul>li a 		{ font: normal 16px/30px rob-l; color: #fff; position: relative;}
.footer2>ul>li:before 	{ content: ''; width: 40px; height: 40px; display: inline-block; vertical-align: middle; border-radius: 50%; margin-right: 10px;}
.footer2>ul>li:nth-child(2),
 .footer2>ul>li:nth-child(3){ margin-left: 60px;}
.footer2>ul>li:nth-child(1):before { background: url(images/icone-plan.svg) 50% no-repeat #fff;}
.footer2>ul>li:nth-child(2):before { background: url(images/icone-tel3.svg) 50% no-repeat #fff;}
.footer2>ul>li:nth-child(3):before { background: url(images/icone-lettre3.svg) 50% no-repeat #fff;}
.footer2>ul>li:nth-child(3) strong{ display: none;}

.toponweb				{ display:block; width: 120px; position:fixed; right:0; bottom:-65px; z-index:85; transition:bottom 400ms ease-in-out;}
.toponweb span			{ width:auto; height:60px; display:block; padding:20px; position: relative;}
.toponweb span:after    { content: ''; background: #255519; width: 100%; position: absolute; opacity: .75; top: 0; bottom: 0; left: 0;}
.toponweb img			{ width:74px; height:auto; display:block; margin:0 auto; position: relative; z-index: 10;}
.toponweb.show			{ bottom:-4px;}
.plan					{ width:100%; position:relative; padding:13px 60px; background:#ebebeb; text-align:center; overflow:hidden; display:none;}
.plan a					{ padding:0 8px; color:#503831;}

/* RESPONSIVE */
@media (max-width:1200px){
    .footer 						{ max-width: 100%; padding: 35px 40px 80px;}
    .social 						{ left: 40px;}
    .scroll 						{ right: 40px;}
	.footer2 > ul > li:nth-child(2),
	.footer2 > ul > li:nth-child(3) { margin-left: 20px;}
}
@media (max-width:980px){
  .footer2 > ul > li                { display: block;}
  .footer2 > ul > li::before        { display: none;}
  .footer2 > ul > li:nth-child(2), 
  .footer2 > ul > li:nth-child(3)   { margin-left: 0;}
  .footer ul > li                   { display: block;}
  .footer2 > ul > li::before        { display: none;}
  .footer2 > ul > li a              { position: relative; padding: 12px 0 0; display: block; font-family: rob-m;}
  .footer2 > ul > li:nth-child(1)   { margin-bottom: 21px;}
  .footer2 > ul > li:nth-child(2), 
  .footer2 > ul > li:nth-child(3)   { background: #255519; width: 210px; height: 55px; margin: 0 auto 20px;}
  .footer2 > ul > li:nth-child(2) a:before { content: ''; background: url(images/icone-tel.svg) 50% no-repeat; width: 15px; height: 15px; display: inline-block; vertical-align: middle; margin-right: 20px;}
  .footer2 > ul > li:nth-child(3) a:before { content: 'Envoyer un Email'; background: url(images/icone-lettre2.svg) 0 50% no-repeat; width: 15px; height: 15px; display: initial; vertical-align: middle; margin-right:0; padding-left: 30px;}
  .footer2>ul>li:nth-child(3) em,
  .footer1 div span, .footer2 span,
     .open-plan                     { display: none;}
    .social a                       { margin-right: 0;}
}
@media (max-width:600px){
    .footer 						{ padding:43px 20px 70px 20px;}
    .logo-foot 						{ display: block; width: 185px; height: 43px; margin-bottom: 15px;}
    .logo-foot img 					{ display:block; width:100%; height:auto;}
    .footer2 						{ padding: 0; text-align: left;}
    .social 						{ top: 0; left: 0; margin: 20px 0 0 0; position: relative; text-align: center;}
    .social a 						{ display: inline-block; float: none;}
    .scroll 						{ display: none;}
}

/* HOVER EFFECTS */
@media (min-width:1201px){
	body a, body a span, span:before, span:after, a:after, a:before, .link, .sub, .slick-prev, .slick-next, .slick-dots button, .service>a>.service-image, .gal .txt, .gal, .service-image:before  { -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out;}
   a:hover, .footer2>ul>li a:hover	{ color:#6eaa28;}
   .menuMobile						{ display:none;}
   .menu .vueMobile				    { display:none;}
   .menu	 						{ display:block !important;}
   .logo:hover						{ opacity:.5;}
   .headerTop .right a:hover 		{ color:#437f34;}
   .headerTop .tel:hover			{ background-color: #6eaa28; color:#fff !important;}
   .rs:hover,.blocContact> div>a:hover{ background-color:#6eaa28 !important;}
   .bloc-real-coor .left .link:hover{ background-color: #503831;}
   .menu li:hover>a 				{ color:#437f34;}
   .menu .sub a:hover 				{ background:#6eaa28; color:#fff;}
   .social a:hover					{ background-color:#6eaa28;}
   .link:hover, .blocContact 
    .txt-right>a:hover 				{ background-color:#6eaa28; color:#fff;}
   .service>a:hover>.service-image:before { opacity: 1;}
   .service>a:hover .link           { background-color: #503831 !important;}
   .scroll:hover					{ background-color: #437f34; border-color: #437f34;}
   .slick-prev:hover, 
   .slick-next:hover                { background-color: #503831; }
   .gal:hover .txt, .bloc:hover .txt{ opacity: 0 !important;}
   .gal:hover .hide,
    .bloc:hover .hide               { opacity: 1;}
   .logo-foot:hover					{ opacity: .5;}
   .toponweb:hover span:after       { background:#6eaa28; opacity: 1;}
}

/* SLICK */
.slick-slider					{ position:relative; display:block; -moz-box-sizing:border-box; box-sizing:border-box; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none; -webkit-touch-callout:none; -khtml-user-select:none; -ms-touch-action:pan-y; touch-action:pan-y; -webkit-tap-highlight-color:transparent;}
.slick-list						{ position:relative; display:block; overflow:hidden; margin:0; padding:0;}
.slick-list:focus				{ outline:none;}
.slick-list.dragging			{ cursor:pointer; cursor:hand;}
.slick-slider .slick-track,
.slick-slider .slick-list		{ -webkit-transform:translate3d(0 0,0); -moz-transform:translate3d(0,0,0); -ms-transform:translate3d(0,0,0); -o-transform:translate3d(0,0,0); transform:translate3d(0,0,0);}
.slick-track					{ position:relative; top:0; left:0; display:block;}
.slick-track:before, 
.slick-track:after 				{ display:table; content: '';}
.slick-track:after				{ clear:both;}
.slick-loading .slick-track		{ visibility:hidden;}
.slick-slide					{ display:none; float:left; height:100%; min-height:1px;}
[dir='rtl'] .slick-slide		{ float:right;}
.slick-slide img				{ display:block;}
.slick-slide.slick-loading img	{ display:none;}
.slick-slide.dragging img		{ pointer-events:none;}
.slick-initialized .slick-slide	{ display:block;}
.slick-loading .slick-slide		{ visibility:hidden;}
.slick-vertical .slick-slide 	{ display:block; height:auto; border:1px solid transparent;}
.slick-prev 					{ width:55px; height:55px; background:url(images/arrow-left.svg) #fff 50% 50% no-repeat; position:absolute; left: inherit !important; right:55px; bottom: 0; z-index:40; cursor:pointer; text-indent:-9999px; border:0; padding:0;}
.slick-next 					{ width:55px; height:55px; background:url(images/arrow-right.svg) #fff 50% 50% no-repeat; position:absolute; right:0; bottom: 0; z-index:40; cursor:pointer; text-indent:-9999px; border:0; padding:0;}
.slick-dots						{ width:100%; height:15px; position:absolute; top: 100%; left:0; text-align:center; padding:0; line-height:0; z-index:40;}
.slick-dots	li					{ display:inline-block; height:20px; margin:0 8px;}
.slick-dots button				{ display:block; width:15px; height:15px; text-indent:-9999px; background:#fff; padding:0; border:1px solid #6eaa28; cursor:pointer;}
.slick-active button			{ background:#503831; border:1px solid #503831;}
</style>