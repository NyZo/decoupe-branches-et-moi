<!-- FOOTER START -->
<footer id="footer">
    <div class="footer">
        <div class="footer1">
            <a href="homepage.php" class="logo-foot" title="Branches et moi"><span>Branches et moi</span>Baptiste Fosty</a>
            <div>BE0806.538.172<span> | </span><a class="open-plan" title="Plan du site">Plan du site</a></div>
        </div>
        <div class="social clr">
            <a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook rs">Facebook</a>
        </div>
        <div class="footer2">
            <ul>
                <li>Rue Delcour, 55 - 5520 Onhaye</li>
                <li><a href="tel:0494194706" title="0494 / 19 47 06">0494 / 19 47 06</a></li>
                <li><a href="mailto:branchesetmoi@yahoo.com" title="branchesetmoi@yahoo.com"><em>branchesetmoi@yahoo.com</em></a></li>
            </ul>
        </div>
        <a href="header" class="scroll rs" title="Allez vers le haut"></a>
        <a href="http://www.toponweb.be/" title="Réalisé par Toponweb" target="_blank" class="toponweb">
            <span><img src="images/toponweb.svg" alt="Top On Web" /></span>
        </a>
    </div>

    <div class="plan">
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
    </div>
</footer>    
<!-- FOOTER END -->