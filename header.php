<!-- HEADER START -->
<header id="header" <?php if ($namePage != "pageAccueil") echo("class='page'"); ?>>
    <div class="headerTop">
        <div class="wrapMenuMobile"><div class="menuMobile"><span>MENU</span><div></div></div></div>
        <div class="logo-slogan">
            <a href="homepage.php" class="logo" title="Branche et moi">
                <img src="images/logo.png" alt="Branche et moi">
            </a>
        </div>
        <div class="right">
            <div class="coord-top clr">
                <a href="tel:0494194706" title="0494 / 19 47 06" class="tel">0494 / 19 47 06</a>
                <?php if ($namePage == "pageAccueil") { ?>
                    <h1 class="slogan">Grimpeur élagueur - Entretien et aménagement de jardin</h1>
                <?php } else { ?>
                    <div class="slogan">Grimpeur élagueur - Entretien et aménagement de jardin</div>
                <?php } ?>
            </div>
            <div class="clr"></div>
            <div class="r-sociaux">
                <a href="#" title="Rejoignez-nous sur Facebook" target="_blank" class="rs facebook">Facebook</a>
                <a href="mailto:contactinfo.ej@gmail.com" title="contactinfo.ej@gmail.com" class="rs lettre">contactinfo.ej@gmail.com</a>
            </div>
            <nav class="menu">
                <ul>
                    <li <?php if ($namePage == "pageAccueil") echo (" class='active'"); ?>><a href="homepage.php" title="Accueil">Accueil</a></li>
                    <li <?php if ($namePage == "page...") echo (" class='active'"); ?>><a href="page.php" title="Présentation">Présentation</a></li>
                    <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="#" title="Services">Services</a><i></i>
                        <ul class="sub">
                            <li class="vueMobile"><a href="#" title="Vue d'ensemble">Vue d'ensemble</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Abattage – élagage">Abattage – élagage</a></li>
                            <li class="active" <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Création et entretien de jardin">Création et entretien de jardin</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Vergers et potagers">Vergers et potagers</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Traction animale">Traction animale</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Bois de chauffage">Bois de chauffage</a></li>
                        </ul>
                    </li>
                    <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="#" title="Réalisations">Réalisations</a></li>
                    <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="#" title="Contact">Contact</a></li>
                    <li class="social">
                        <a href="#" title="Rejoignez-nous sur Facebook" target="_blank" class="rs facebook">Facebook</a>
                        <a href="mailto:contactinfo.ej@gmail.com" title="contactinfo.ej@gmail.com" class="rs lettre">contactinfo.ej@gmail.com</a>
                    </li>
                </ul>
            </nav>
            <div class="clr"></div>      
        </div>
    </div>    
    <div id="slider">
        <?php if ($namePage == "pageAccueil") { ?>
            <div class="banner1">
                <div class="bannerTxt">
                    <h2 class="ban-txt">Entreprise de jardinage et élagage <span>dans la région de Onhaye en province de Namur</span></h2>
                    <a href="blocService" class="link" title="Découvir nos services">Découvir nos services</a>
                </div>        
            </div>
        <?php } ?>
    </div>
</header>
<!-- HEADER END -->