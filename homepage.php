<?php $namePage="pageAccueil"; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Homepage | Branches et moi</title>
        <meta name="description" content="Homepage | Branches et moi" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
        <div id="wrapper">
            <?php include "header.php";?>
            <main id="homepage">
                <div class="blocPresentation wrap clr">
                    <div class="left">
                        <img src="images/img-presentation.jpg" alt="Branche et moi"/>
                    </div>
                    <div class="right">
                        <h2 class="titre">Baptiste Fosty</h2>
                        <h3 class="stitre">Arboriste et grimpeur<span> -</span> élagueur qualifié</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                        <a href="#" class="link" title="En savoir plus">En savoir plus</a>
                    </div>
                </div>
                <div class="blocService" id="blocService">
                    <div class="wrap">
                        <div class="slide-service clr">
                            <div class="service service1">
                                <a href="#" title="Abattage & élagage">
                                    <span class="service-image"><img src="images/service1.svg" alt="Abattage & élagage"/></span>
                                    <h3 class="titre">Abattage &<br/>élagage</h3>
                                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim</p>
                                    <span class="link">En savoir plus</span>
                                </a>
                            </div>
                            <div class="service service2">
                                <a href="#" title="Création & entretien de jardin">
                                    <span class="service-image"><img src="images/service2.svg" alt="Création & entretien de jardin"/></span>
                                    <h3 class="titre">Création &<br/>entretien de jardin</h3>
                                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim</p>
                                    <span class="link">En savoir plus</span>
                                </a>
                            </div>
                            <div class="service service3">
                                <a href="#" title="Vergers & potagers">
                                    <span class="service-image"><img src="images/service3.svg" alt="Vergers & potagers"/></span>
                                    <h3 class="titre">Vergers &<br/>potagers</h3>
                                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim</p>
                                    <span class="link">En savoir plus</span>
                                </a>
                            </div>
                            <div class="service service4">
                                <a href="#" title="Traction animale">
                                    <span class="service-image"><img src="images/service4.svg" alt="Traction animale"/></span>
                                    <h3 class="titre">Traction<br/>animale</h3>
                                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim</p>
                                    <span class="link">En savoir plus</span>
                                </a>
                            </div>
                            <div class="service service3">
                                <a href="#" title="Vergers & potagers">
                                    <span class="service-image"><img src="images/service3.svg" alt="Vergers & potagers"/></span>
                                    <h3 class="titre">Vergers &<br/>potagers</h3>
                                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim</p>
                                    <span class="link">En savoir plus</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blocAtout">
                    <div class="slideAtout wrap">
                        <div class="atout atout-bg">
                            <span class="picto"><img src="images/atout1.svg" alt="Diplômé et qualifié en horticulture"/></span>
                            <span>Diplômé et qualifié</span> en horticulture
                        </div>
                        <div class="atout">
                            <span class="picto"><img src="images/atout2.svg" alt="Qualité et suivi du travail"/></span>
                            <span>Qualité et suivi</span> du travail  
                        </div>
                        <div class="atout atout-bg">
                            <span class="picto"><img src="images/atout3.svg" alt="Equipé pour tous travaux"/></span>
                            <span>Equipé</span> pour tous travaux
                        </div>
                        <div class="atout">
                            <span class="picto"><img src="images/atout4.svg" alt="Respect de l’écologie"/></span>
                            <span>Respect</span> de l’écologie
                        </div>
                        <div class="atout atout-bg">
                            <span class="picto"><img src="images/atout5.svg" alt="Devis gratuit"/></span>
                            <span>Devis</span> gratuit
                        </div>
                    </div>
                </div>
                <div class="bloc-real-coor">
                    <div class="blocContact">
                        <div class="wrap">
                            <a href="tel:0494194706" class="phone" title="0494 / 19 47 06">0494 / 19 47 06</a>
                            <a href="mailto:branchesetmoi@yahoo.com" class="mail" title="Envoyer un Email">Envoyer un Email</a>
                        </div>
                    </div>
                    <div class="wrap clr">
                        <div class="left">
                            <a href="#" class="link" title="Découvrir nos réalisations"><span>Découvrir </span>nos réalisations</a>
                            <div class="slide-real">
                                <div><img src="images/img-real-1.jpg" alt="Branche et moi" /></div>
                                <div><img src="images/img-real-2.jpg" alt="Branche et moi" /></div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="coor">
                                <h2 class="titre">Actif dans un rayon de 30km</h2>
                                <h3 class="stitre">Autour de Mettet</h3>
                                <p>Anhée, Dinant, Floreffe, Florennes, Fosses-la-Ville, Gerpinnes, Hastière, Namur, Onhaye, Philippeville, Profondeville, Saint-Gérard, Yvoir</p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <?php include "footer.php";?>
        </div>
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
			$(document).ready(function() {	
				/* SLIDER SERVICE */
                $('.slide-service').slick({
                    dots:true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    responsive: [
                        {
                        breakpoint:1201,
                        settings: { slidesToShow:3 }
                        },{
                        breakpoint:981,
                        settings: { slidesToShow:2 }
                        },{
                        breakpoint:601,
                        settings: { slidesToShow:1 }
                        }
                    ]
                    });
                    
                /* SLIDER ATOUT */
                    $('.slideAtout').slick({
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        responsive: [
                                { 
                                    breakpoint: 1245,
                                    settings: {
                                        slidesToShow: 4,
                                    } 
                                },
                                { 
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 3,
                                    } 
                                },
                                { 
                                    breakpoint: 760,
                                    settings: {
                                        slidesToShow: 2,
                                    }
                                },
                                { 
                                    breakpoint: 601,
                                    settings: {
                                        slidesToShow: 1,
                                    }
                                }
                        ]
                });
                
                /* SLIDER REALISATION */
                $('.slide-real').slick({
                    arrows: true
                });
				
			});
        </script>
    </body>
</html>