<?php $namePage="page..."; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Page | Branches et moi</title>
        <meta name="description" content="Page | Branchees et moi" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
    	<div id="wrapper">
			<?php include "header.php";?>
            
            <main class="pageContent">
				<div class="wrap">
					<div class="blocTop">
					    <div class="bredCrumb">
                            <a href="homepage.php" title="Accueil">Accueil</a> <a href="page.php" class="active" title="Présentation">Présentation</a>
                        </div>
				        <div class="titrePage">Présentation<span class="sousTitre">Branches et moi</span></div>
						<h1 class="sousTitre">Baptiste Fosty, diplômé de l’Institut Technique Horticole de Gembloux</h1>
						<h2 class="chapo">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostru ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</h2>
					</div>   
					<div class="blocText blocText1">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>    
                    </div>
					<div class="blocPhoto clr">
						<div class="left">
							<div class="slide-photo">
								<div><img src="images/photo-page1.jpg" alt="Branches et moi"></div>
								<div><img src="images/photo-page2.jpg" alt="Branches et moi"></div>
							</div>
						</div>
						<div class="right">
							<h3 class="sousTitre">Eaque ipsa quae ab illo inventore veritatis et quasi architecto</h3>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
						</div>
					</div>
				</div>
				<div class="blocContact">
					<div class="wrap">
						<span class="txt-left">Vous avez un projet ? Merci de nous contacter</span>
						<span class="txt-right">
							<a href="tel:0494194706" class="phone" title="0494 / 19 47 06">0494 / 19 47 06</a>
							<a href="mailto:branchesetmoi@yahoo.com" class="mail" title="Envoyer un Email">Envoyer un Email</a>
						</span>
					</div>
				</div>
				<div class="wrap">
					<div class="blocText blocText2">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo :</p>
							<ul>
								<li>Excepteur sint occaecat non<span> proident,</span></li>
								<li>Sunt in culpa qui officia deserunt<span> mollit anim,</span></li>
								<li>Sed ut perspiciatis unde omnis<span> iste natus.</span></li>
								<li>Sit voluptatem accusantium<span> doloremque,</span></li>
								<li>Totam rem aperiam, eaque ipsa<span> quae ab illo.</span></li>
							</ul>
						<div class="sousTitre">Voluptatem accusantium dolor que laudantium</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi<span> ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</span></p>
					</div>
				</div>
                <div class="blocGalerie clr">
                    <div class="slide-galerie">
                        <div class="bloc bloc1">
                            <a href="#" title="Nos services">
                                <img src="images/img1.jpg" alt="Nos services">
                                <span class="txt">Nos services</span>
                                <span class="hide">
                                    <strong>Nos services</strong>
                                    <p>Sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco</p>
                                    <em></em>
                                </span>
                            </a>	
                        </div>
                        <div class="bloc bloc2">
                            <a href="#" title="Nos réalisations">
                                <img src="images/img2.jpg" alt="Nos réalisations">
                                <span class="txt">Nos réalisations</span>
                                <span class="hide">
                                    <strong>Nos réalisations</strong>
                                    <p>Sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco</p>
                                    <em></em>
                                </span>
                            </a>	
                        </div>
                        <div class="bloc bloc3">
                            <a href="#" title="Nous contacter">
                                <img src="images/img3.jpg" alt="Nous contacter">
                                <span class="txt">Nous contacter</span>
                                <span class="hide">
                                    <strong>Nous contacter</strong>
                                    <p>Sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco</p>
                                    <em></em>
                                </span>
                            </a>	
                        </div>
                    </div>
                </div>  
            </main>
            
            <?php include "footer.php";?>
        </div>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
			$(document).ready(function() {
				// SLIDE PHOTO
				$('.slide-photo').slick({
					arrows: true
				});
				
				// SLIDE GALERIE
				$('.slide-galerie').slick({
					slidesToShow: 3,
					slidesToScroll: 1,
                    dots:true,
                    responsive: [
                        {
                        breakpoint:1024,
                        settings: { slidesToShow:2}
                        },{
                        breakpoint:601,
                        settings: { slidesToShow:1 }
                        }
                    ]
				});
			});
        </script>
    </body>
</html>